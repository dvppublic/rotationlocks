//
//  RotationViewController.swift
//  TestRotationsLocks
//
//  Created by Dimitar V. Petrov on 10/18/16.
//  Copyright © 2016 Musala Soft. All rights reserved.
//

import UIKit

enum Orientation {
    case Landscape
    case Portrait
    
    mutating func changeOrientation() {
        if self == .Portrait {
            self = .Landscape
        }
        else {
            self = .Portrait
        }
    }
    
    func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        switch self {
        case .Landscape:
            return .Landscape
        case .Portrait:
            return .Portrait
        }
    }
    
    func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        switch self {
        case .Landscape:
            return UIInterfaceOrientation.LandscapeLeft
        case .Portrait:
            return .Portrait
        }
    }
}

var currentOrientation: Orientation = .Portrait

class RotationViewController: UINavigationController {

    //    Returns a Boolean value indicating whether the view controller's contents should auto rotate.
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    //    Returns all of the interface orientations that the view controller supports.
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return currentOrientation.supportedInterfaceOrientations() //UIInterfaceOrientationMask.All
    }
    
    //    Returns the interface orientation to use when presenting the view controller.
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.Portrait
    }
    
}
