//
//  ViewController.swift
//  TestRotationsLocks
//
//  Created by Dimitar V. Petrov on 10/17/16.
//  Copyright © 2016 Musala Soft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    /*
    //    Returns a Boolean value indicating whether the view controller's contents should auto rotate.
    override func shouldAutorotate() -> Bool {
        return true
    }
    
//    Returns all of the interface orientations that the view controller supports.
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return currentOrientation.supportedInterfaceOrientations() //UIInterfaceOrientationMask.All
    }
    
//    Returns the interface orientation to use when presenting the view controller.
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.Portrait
    }*/
    
    @IBAction func forceChangeOrientations(sender: AnyObject) {
        currentOrientation.changeOrientation()

        let value = currentOrientation.preferredInterfaceOrientationForPresentation().rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        UIViewController.attemptRotationToDeviceOrientation()
    }
    
}

