//
//  PushedViewController.swift
//  TestRotationsLocks
//
//  Created by Dimitar V. Petrov on 10/18/16.
//  Copyright © 2016 Musala Soft. All rights reserved.
//

import UIKit

class PushedViewController: UIViewController {

    

    @IBAction func forceRotatePushedVC(sender: AnyObject) {
        
        currentOrientation.changeOrientation()
        
        let value = currentOrientation.preferredInterfaceOrientationForPresentation().rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        UIViewController.attemptRotationToDeviceOrientation()
    }
    
    /*
    //    Returns a Boolean value indicating whether the view controller's contents should auto rotate.
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    //    Returns all of the interface orientations that the view controller supports.
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return currentOrientation.supportedInterfaceOrientations() //UIInterfaceOrientationMask.All
    }
    
    //    Returns the interface orientation to use when presenting the view controller.
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return currentOrientation.preferredInterfaceOrientationForPresentation()
    }
 */
}
